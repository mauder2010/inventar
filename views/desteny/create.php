<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Desteny */

$this->title = 'Додати місце';
$this->params['breadcrumbs'][] = ['label' => 'Всі місця', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="desteny-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

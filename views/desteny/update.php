<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Desteny */

$this->title = 'Корегувати: ' . $model->id_destination;
$this->params['breadcrumbs'][] = ['label' => 'Всі місця', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id_destination, 'url' => ['view', 'id' => $model->id_destination]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="desteny-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

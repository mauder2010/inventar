<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Desteny */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="desteny-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name_destinations')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Зберегти', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

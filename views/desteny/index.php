<?php

use yii\helpers\Html;
use yii\grid\GridView;


/* @var $this yii\web\View */
/* @var $searchModel app\models\DestenySerch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Всі місця';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="desteny-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Додати нове місце', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        // 'tableOptions' => [
        //             'class' => 'table table-striped '
        //         ],

        'columns' => [

          [
'class' => 'yii\grid\SerialColumn',
'header'=>'№ з/п',
'contentOptions' => ['style' => 'width:1%;'],

],


            //'id_destination',
            // 'name_destinations',
            [
       'format'=>"ntext", // or other formatter
       'attribute' => 'name_destinations',
       'contentOptions' => ['style' => 'width:40%'], // For TD
       'headerOptions' => ['class' => 'ur-class'] // For TH

   ],

   [
   'class' => 'yii\grid\ActionColumn',
   'header'=>'Операції',
   'headerOptions' => ['width' => '80'],
   'contentOptions' => ['style' => 'width:1%'],
   'template' => '{view} {update} {delete}',
],
        ],
    ]); ?>


</div>

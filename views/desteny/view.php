<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Desteny */

$this->title = $model->id_destination;
$this->params['breadcrumbs'][] = ['label' => 'Всі місця', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="desteny-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Повернутися до корегування', ['update', 'id' => $model->id_destination], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Видалити', ['delete', 'id' => $model->id_destination], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id_destination',
            'name_destinations',
        ],
    ]) ?>

</div>

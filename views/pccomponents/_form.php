<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Pccomponents */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="pccomponents-form">

    <?php $form = ActiveForm::begin(); ?>
<!--
    <?= $form->field($model, 'id')->textInput() ?> -->

    <?= $form->field($model, 'name')->textarea(['rows' => 1]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

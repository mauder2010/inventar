<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Pccomponents */

$this->title = 'Create Pccomponents';
$this->params['breadcrumbs'][] = ['label' => 'Pccomponents', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pccomponents-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

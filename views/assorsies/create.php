<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Assorsies */

$this->title = 'Create Assorsies';
$this->params['breadcrumbs'][] = ['label' => 'Assorsies', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="assorsies-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

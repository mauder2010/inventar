<?php

use yii\helpers\Html;
// use yii\widgets\ActiveForm;
use yii\bootstrap\ActiveForm;
use app\models\Pccomponents;
use yii\helpers\ArrayHelper;
/* @var $this yii\web\View */
/* @var $model app\models\Assorsies */
/* @var $form yii\widgets\ActiveForm */
use kartik\select2\Select2;


$modelcomponents = Pccomponents::find()->all();
$itemscomponents = ArrayHelper::map($modelcomponents,'id','name');
$paramslist = [
    'prompt' => 'Виберіть із списку'
];
?>

<div class="assorsies-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'id_inventar')->textInput() ?>
    <?= $form->field($model, 'id_components')->widget(Select2::classname(), [
        'data' => $itemscomponents,
        'options' => ['placeholder' => 'Виберіть із списку'],
        'pluginOptions' => [
            'allowClear' => true ],]); ?>
  

    <?= $form->field($model, 'ass_description')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

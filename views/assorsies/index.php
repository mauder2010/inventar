<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;
use app\models\Assorsies;
use app\models\Inventar;
use app\models\Pccomponents;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $searchModel app\models\AssorsiesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Assorsies';
$this->params['breadcrumbs'][] = $this->title;

// $arr = ArrayHelper::map(Assorsies::find()->leftJoin('inventar', 'assorsies_pc.id_inventar = inventar.id_n')->all(), 'inventar.id_n', 'inventar.inv_n');
// echo '<pre>';
// print_r($arr);
// echo '</pre>';
?>
<div class="assorsies-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Додати', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $AssorSearch,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            // 'id',
            // 'assorsies_name',
            ['label'=>'Інвентарний №',
             'attribute'=>'id_inventar',
             'value' => 'inventar.inv_n',
             'contentOptions' => ['style' => 'width:200px; white-space: normal;'],

             'filter' =>  Select2::widget([
               'model' => $AssorSearch,
               'attribute' => 'id_inventar',
               'data' => ArrayHelper::map(Assorsies::find()->leftJoin('inventar', 'assorsies_pc.id_inventar = inventar.id_n')->all(), 'inventar.id_n', 'inventar.inv_n'),
               'value' => 'inventar.inv_n',
               'options' => [
                   'class' => 'form-control',
                   'placeholder' => 'Выберіть значення'
               ],
               'pluginOptions' => [
                   'allowClear' => true,
                   'selectOnClose' => true,
               ]
            ])

            ],
            ['label'=>'Тип комлектуючого',
             'attribute'=>'id_components',
             'value' => 'components.name',
             'filter' =>  Select2::widget([
               'model' => $AssorSearch,
               'attribute' => 'id_components',
               'data' => ArrayHelper::map(Pccomponents::find()->all(), 'id', 'name'),
               'value' => 'components.name',
               'options' => [
                   'class' => 'form-control',
                   'placeholder' => 'Выберіть значення'
               ],
               'pluginOptions' => [
                   'allowClear' => true,
                   'selectOnClose' => true,
               ]
           ])
           ],



        ['label'=>'Характеристики',
         'attribute'=>'ass_description',
         'value' => 'ass_description',],


            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>

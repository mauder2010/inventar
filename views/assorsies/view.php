<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Assorsies */

$this->title = $AssorModel->id;
$this->params['breadcrumbs'][] = ['label' => 'Assorsies', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
// \yii\web\YiiAsset::register($this);
?>
<div class="assorsies-view">

    <!-- <h1><?= HTML::encode ($this->title) ?></h1> -->

    <p>
        <?= Html::a('Update', ['update', 'id' => ""], ['class' => 'btn btn-primary']) ?>

        <?= Html::a('Delete', ['delete', 'id' => $AssorModel->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'fdgd',
                'method' => 'post'],

        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $AssorModel,
        'attributes' => [
            'id',
            'components.name',
            'inventar.inv_n',
        ],
    ]) ?>

</div>

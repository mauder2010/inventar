<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Inventar */

$this->title = 'Update Inventar: ' . $model->id_n;
$this->params['breadcrumbs'][] = ['label' => 'Inventars', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id_n, 'url' => ['view', 'id' => $model->id_n]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="inventar-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

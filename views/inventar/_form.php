<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;


$models = app\models\Types::find()->all();
$items = ArrayHelper::map($models,'id_type','type_name');
$params = [
    'prompt' => 'Виберіть із списку'
];
$modelsdest = app\models\Desteny::find()->all();
$itemsdest = ArrayHelper::map($modelsdest,'id_destination','name_destinations');
$paramsdest = [
'prompt' => 'Виберіть із списку'

];


/* @var $this yii\web\View */
/* @var $model app\models\Inventar */
/* @var $form yii\widgets\ActiveForm */
?>


<div class="inventar-form">


    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'inv_n')->textInput() ?>

    <?= $form->field($model, 'des_tz')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'f_price')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'id_destmain')->widget(Select2::classname(), [
        'data' => $itemsdest,
        'options' => ['placeholder' => 'Виберіть із списку'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]); ?>
    <?= $form->field($model, 'id_typemain')->widget(Select2::classname(), [
        'data' => $items,
        'options' => ['placeholder' => 'Виберіть із списку'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]); ?>

    

    <div class="form-group">
        <?= Html::submitButton('Зберегти', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Inventar */

$this->title = 'Додати інвентар';
$this->params['breadcrumbs'][] = ['label' => 'Inventars', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="inventar-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;
use app\models\Types;
use app\models\Desteny;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $searchModel app\models\InventarSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Загальний список';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="inventar-index">


    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Додати дані', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        // 'summary' => false,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'id_n',
            'inv_n',
            'des_tz:ntext',
            'f_price',
            //'dest',
             [
             'label'=>'розміщення',
              'attribute'=>'id_destmain',
              'value' => 'destname.name_destinations',
              //'filter'=>ArrayHelper::map(Desteny::find()->all(), 'id_destination', 'name_destinations'),
              'contentOptions' => ['style' => 'width:200px; white-space: normal;'],
              'filter' =>  Select2::widget([
                'model' => $searchModel,
                'attribute' => 'id_destmain',
                'data' => ArrayHelper::map(Desteny::find()->all(), 'id_destination', 'name_destinations'),
                'value' => 'destname.name_destinations',
                'options' => [
                    'class' => 'form-control',
                    'placeholder' => 'Выберіть значення'
                ],
                'pluginOptions' => [
                    'allowClear' => true,
                    'selectOnClose' => true,
                ]
            ])
        //
        ],
            //'type',
            //'id_typemain',
            [
            'label'=>'Тип техніки',
            'attribute'=>'id_typemain',
            'value' => 'types.type_name',
            'filter' =>  Select2::widget([
              'model' => $searchModel,
              'attribute' => 'id_typemain',
              'data' => ArrayHelper::map(types::find()->all(), 'id_type', 'type_name'),
              'value' => 'types.type_name',
              'options' => [
                  'class' => 'form-control',
                  'placeholder' => 'Выберіть значення'
              ],
              'pluginOptions' => [
                  'allowClear' => true,
                  'selectOnClose' => true,
              ]
            ])
        ],





            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>

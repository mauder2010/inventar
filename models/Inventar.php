<?php

namespace app\models;

use Yii;
use yii\web\NotFoundHttpException;
/**
 * This is the model class for table "inventar".
 *
 * @property int $id_n
 * @property int $inv_n
 * @property string $des_tz
 * @property float $f_price
 * @property string $dest
 * @property string $type
 */
class Inventar extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'inventar';
    }

    /**
     * {@inheritdoc}
     */

     public function getDestname(){
         return $this->hasOne(Desteny::className(), ['id_destination'=>'id_destmain']);
     }
     public function getTypes(){
         return $this->hasOne(Types::className(), ['id_type'=>'id_typemain']);

     }
     public function getAss()
     {
         return $this->hasMany(Assorsies::className(), ['id_inventar' => 'id_n']);
     }
    public function rules()
    {
        return [
            [['inv_n', 'des_tz', 'f_price'], 'required'],
            [['inv_n' , 'id_typemain', 'id_destmain'], 'integer'],
            [['des_tz'], 'string'],
            [['f_price'], 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_n' => 'Id N',
            'inv_n' => 'Інвентарний №',
            'des_tz' => 'Опис',
            'f_price' => 'Первісна вартість',
            'id_destmain'=>'Розміщеня',
            'id_typemain' => 'Тип ОЗ'
        ];
    }


}

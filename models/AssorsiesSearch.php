<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Assorsies;

/**
 * AssorsiesSearch represents the model behind the search form of `app\models\Assorsies`.
 */
class AssorsiesSearch extends Assorsies
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'id_inventar','id_components'], 'integer'],
            // [['assorsies_name'], 'safe'],
            [['ass_description'], 'safe'],

        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Assorsies::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'id_inventar' => $this->id_inventar,
            'id_components'=>$this->id_components,
            
            // 'ass_description' =>$this->ass_description,
        ]);

        $query->andFilterWhere(['like', 'assorsies_name', $this->assorsies_name]);
        $query->andFilterWhere(['like', 'ass_description',$this->ass_description]);

        // $query->andFilterWhere(['like'], '\'ass_description' =>$this->ass_description\')

        return $dataProvider;
    }
}

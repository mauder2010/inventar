<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "desteny".
 *
 * @property int $id_desination
 * @property string $name_destinations
 */
class Desteny extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'desteny';
    }

    // public function getKab(){
    //     $this->hasOne(Inventar::className(), ['id_destination'=>'id_destmain']);
    // }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name_destinations'], 'required'],
            [['name_destinations'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            // 'id_destination' => 'Id Destination',
            'name_destinations' => 'Місце розміщення техніки',
        ];
    }
}

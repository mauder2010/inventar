<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Inventar;

/**
 * InventarSearch represents the model behind the search form of `app\models\Inventar`.
 */
class InventarSearch extends Inventar
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_n', 'inv_n','id_typemain', 'id_destmain'], 'integer'],
            [['des_tz'], 'safe'],
            [['f_price'], 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Inventar::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id_n' => $this->id_n,
            'inv_n' => $this->inv_n,
            'f_price' => $this->f_price,
            'id_destmain'=> $this->id_destmain,
            'id_typemain'=>$this->id_typemain,



        ]);

        $query->andFilterWhere(['like', 'des_tz', $this->des_tz]);
        //    ->andFilterWhere(['like', 'id_typemain', $this->id_typemain]);

        return $dataProvider;
    }
}

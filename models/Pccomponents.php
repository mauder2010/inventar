<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "pccomponents".
 *
 * @property int $id
 * @property string $name
 *
 * @property Assorsies $id0
 */
class Pccomponents extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'pccomponents';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name'], 'string'],
            [['id'], 'exist', 'skipOnError' => true, 'targetClass' => Assorsies::className(), 'targetAttribute' => ['id' => 'id_components']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
        ];
    }

    /**
     * Gets query for [[Id0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getId0()
    {
        return $this->hasMany(Assorsies::className(), ['id_components' => 'id']);
    }
}

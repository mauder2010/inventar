<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "assorsies_pc".
 *
 * @property int $id
 * @property string $assorsies_name
 * @property int $id_inventar
 *
 * @property Inventar $inventar
 */
class Assorsies extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'assorsies_pc';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
             [[/*'assorsies_name', */'id_inventar', 'ass_description'], 'required'],
            [['assorsies_name', 'ass_description'], 'string'],
            [['id_inventar','id_components'], 'integer'],
            [['id_inventar'], 'exist', 'skipOnError' => true, 'targetClass' => Inventar::className(), 'targetAttribute' => ['id_inventar' => 'id_n']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'assorsies_name' => 'Assorsies Name',
            'id_inventar' => 'Інвентарний №',
            'ass_description' => 'Характеристики',
            'id_components' =>'Тип комлектуючого'

        ];
    }

    /**
     * Gets query for [[Inventar]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getInventar()
    {
        return $this->hasOne(Inventar::className(), ['id_n' => 'id_inventar']);
    }
    public function getComponents()
    {
        return $this->hasOne(Pccomponents::className(), ['id' => 'id_components']);
    }
}
